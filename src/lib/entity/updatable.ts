export class Updatable {
  uuid: string;
  whenCreated: number;
  whenUpdated: number;

  getLabels(): { [key: string]: string; } {
    return {
      uuid: 'base.uuid',
      whenCreated: 'base.created',
      whenUpdated: 'base.updated',
    };
  }
}
