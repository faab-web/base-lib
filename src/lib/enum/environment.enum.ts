export enum EnvironmentEnum {
  LOCAL = 'local',
  DEVELOPMENT = 'development',
  PRODUCTION = 'production'
}
