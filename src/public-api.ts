/*
 * Public API Surface of base-lib
 */
export * from './lib/service/base-lib.service';
export * from './lib/entity/updatable';
export * from './lib/enum/environment.enum';

export * from './lib/base-lib.module';
